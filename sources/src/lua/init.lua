-- WiFi module
wifi.setmode(wifi.STATION)
wifi.sta.autoconnect(0)
wifi.sta.setip({ ip = "192.168.1.34", netmask = "255.255.255.0", gateway = "192.168.1.1" })
--wifi.sta.config("Alkor2", "enterprise_pc100264")
wifi.sta.connect()

tmr.alarm(0, 500, 1, function()
    if wifi.sta.status() >= 5 then
        tmr.stop(0)
        print("Config done, got an IP:")
        print(wifi.sta.getip())
        dofile("mainLoop.lua")
    end
end)
