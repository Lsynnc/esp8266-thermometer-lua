tPin = 3
--ip = "192.168.1.33"
--port = 8081
ip = "184.106.153.149"
port = 80

-- gpio2 as input
gpio.mode(4, gpio.OUTPUT)

require('ds18b20')
ds18b20.setup(tPin)
ds18b20.read();

function sendData()
    -- Close previous connection, if any
    if conn ~= nil then
        conn:close()
    end

    t = ds18b20.read()
    print("Temp (C):")
    print(t)
    conn = net.createConnection(net.TCP, 0)
    conn:on("sent", function(conn)
        print("Sent ok")
        conn:close()
    end)
    conn:connect(port, ip)
    conn:send("GET /update?key=C42RYCXKN8ZCQ4YI&field1=")
    conn:send(t)
    conn:send(" HTTP/1.1\r\nHost: api.thingspeak.com\r\nAccept: */*\r\n\r\n")
end

sendData()
tmr.alarm(1, 30 * 1000, 1, sendData)
